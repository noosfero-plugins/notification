require_dependency 'profile'

Profile.class_eval do
  has_many :notifications, -> { order(:start_date) }, :source => 'articles', :class_name => 'NotificationPlugin::NotificationContent'
  has_many :lobby_notes, -> { order(:start_date) }, :source => 'articles', :class_name => 'NotificationPlugin::LobbyNoteContent'
end
